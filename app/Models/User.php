<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Faker\Factory as Faker;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public function faker()
    {
        return Faker::create("id_ID");
    }

    public function createNewUser($nrp, $name)
    {
        $createNewUser = new User();
        $createNewUser->nrp = $nrp;
        $createNewUser->uuid = $this->faker()->uuid;
        $createNewUser->name = $name;
        $createNewUser->save();
        return $createNewUser;
    }

    public function todolists()
    {
        return $this->hasMany(Todolist::class, "user_id");
    }

    public static function findUserByNrp($nrp)
    {
        return User::where("nrp", $nrp)->first();
    }

    public static function findUserByUUID($uuid)
    {
        return User::where("uuid", $uuid)->first();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

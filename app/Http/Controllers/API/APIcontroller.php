<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Todolist;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class APIcontroller extends Controller
{
    public function createUser(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => \true,
        ];
        $allowedRequestRules = [
            "nrp" => ["required", "numeric","unique:users,nrp"],
            "name" => ["required", "max:50"],
        ];
        $validator = Validator::make($request->all(), $allowedRequestRules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["message"] = "please check your body request";
            $response["errors"] = $validator->errors();
        } else {
            $nrp = $request->nrp;
            $name = $request->name;

            $newUser = new User();
            $response["data"] = $newUser->createNewUser($nrp, $name);;
            $response["message"] = "success to create new user";
        }

        return \response()->json($response, $response["code"]);
    }

    public function updateUser(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => \true
        ];

        $allowedRequestRules = [
            "uuid" => ["required"],
            "name" => ["required", "max:50"],
        ];
        $validator = Validator::make($request->all(), $allowedRequestRules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["message"] = "please check your body request";
            $response["errors"] = $validator->errors();
        } else {
            $uuid = $request->uuid;
            $name = $request->name;

            $findUser = User::findUserByUUID($uuid);
            if(!$findUser) {
                $response["code"] = 400;
                $response["status"] = \false;
                $response["message"] = "uuid user of {$uuid} is not found";
            } else {
                $findUser->name = $name;
                $findUser->update();

                $response["message"] = "success to update user data";
                $response["data"] = $findUser;
            }
        }


        return \response()->json($response, $response["code"]);
    }

    public function deleteUser(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
        ];

        $allowedRequestRules = [
            "uuid" => ["required"],
        ];

        $validator = Validator::make($request->all(), $allowedRequestRules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["message"] = "please check your body request";
            $response["errors"] = $validator->errors();
        } else {
            $uuid = $request->uuid;
            $findUser = User::findUserByUUID($uuid);
            if(!$findUser) {
                $response["code"] = 400;
                $response["status"] = \false;
                $response["message"] = "uuid user of {$uuid} is not found";
            } else {
                $findUser->delete();

                $response["message"] = "success to delete user";
                $response["data"] = $findUser;
            }
        }

        return \response()->json($response, $response["code"]);
    }

    public function getUser(Request $request)
    {
        $user = User::all();
        $response = [
            "code" => 200,
            "status" => true,
            "message"  => "sucess to get users",
            "total_data" => count($user),
            "data" => $user,
        ];
        return \response()->json($response, $response["code"]);
    }

    public function createTodolist(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
        ];

        $rules = [
            "title" => ["required"],
            "description" => ["required"],
            "user_id" => ["required"],
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["errors"] = $validator->errors();
            $response["message"] = "please check the parameters";
        } else {
            $findUser = User::find($request->user_id);
            if(!$findUser) {
                $response["code"] = 400;
                $response["status"] = \false;
                $response["message"] = "user not found";
            } else {
                $createTodolist = new Todolist();
                $createTodolist->user_id = $request->user_id;
                $createTodolist->title = $request->title;
                $createTodolist->description = $request->description;
                $createTodolist->save();

                $response["message"] = "success to create todo list";
            }
        }

        return \response()->json($response, $response["code"]);
    }

    public function updateTodolist(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
        ];

        $rules = [
            "title" => ["required"],
            "description" => ["required"],
            "todolist_id" => ["required"],
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["errors"] = $validator->errors();
            $response["message"] = "please check the parameters";
        } else {
            $findTodolist = Todolist::find($request->todolist_id);
            if(!$findTodolist) {
                $response["code"] = 400;
                $response["status"] = \false;
                $response["message"] = "todolist not found";
            } else {
                $findTodolist->title = $request->title;
                $findTodolist->description = $request->description;
                $findTodolist->update();

                $response["message"] = "success to update todolist";
            }
        }

        return \response()->json($response, $response["code"]);
    }

    public function deleteTodolist(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
        ];

        $rules = [
            "todolist_id" => ["required"],
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {
            $response["code"] = 400;
            $response["status"] = \false;
            $response["errors"] = $validator->errors();
            $response["message"] = "please check the parameters";
        } else {
            $findTodolist = Todolist::find($request->todolist_id);
            if(!$findTodolist) {
                $response["code"] = 400;
                $response["status"] = \false;
                $response["message"] = "todolist not found";
            } else {
                $findTodolist->delete();

                $response["message"] = "success to delete todolist";
            }
        }

        return \response()->json($response, $response["code"]);
    }

    public function getTodolist($userId)
    {
        $user = User::find($userId);
        if(!$user) {
            return \response([
                "status" => \false,
                "code" => 400,
                "message" => "user not found",
            ], 400);
        }
        $response = [
            "code" => 200,
            "status" => true,
            "message"  => "sucess to get users",
            "data_user" => $user,
            "todolist" => $user->todolists,
        ];
        return \response()->json($response, $response["code"]);
    }
}

<?php

use App\Http\Controllers\API\APIcontroller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get("testing", function() {
    $response = [
        "code" => 200,
        "status" => true,
        "message" => "Hello world",
    ];

    return response()->json($response, $response["code"]);
});


// ----  API BUAT DATA USER ---- \\
Route::post("user", [APIcontroller::class, "createUser"]);

// ---- API AMBIL DATA USER ---- \\
Route::get("user", [APIcontroller::class, "getUser"]);

// ---- API EDIT DATA USER ---- \\
Route::put("user", [APIcontroller::class, "updateUser"]);

// ---- API HAPUS DATA USER ---- \\
Route::delete("user", [APIcontroller::class, "deleteUser"]);

Route::prefix("todolist")->group(function() {
    Route::controller(APIcontroller::class)->group(function() {
        Route::get("{user_id}","getTodolist");
        Route::post("/","createTodolist");
        Route::put("/","updateTodolist");
        Route::delete("/","deleteTodolist");
    });
});